-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 179.215.137.146    Database: cadPlanta
-- ------------------------------------------------------
-- Server version	8.0.20-0ubuntu0.19.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dtbAbelha`
--

DROP TABLE IF EXISTS `dtbAbelha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dtbAbelha` (
  `id_Abelha` int NOT NULL AUTO_INCREMENT,
  `ds_abelha` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_Abelha`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dtbAbelha`
--

LOCK TABLES `dtbAbelha` WRITE;
/*!40000 ALTER TABLE `dtbAbelha` DISABLE KEYS */;
INSERT INTO `dtbAbelha` VALUES (1,'Iraí (Nannotrigona testaceicornes)'),(2,'Uruçu (Melipona scutellaris)'),(3,'Uruçu-Amarela (Melipona rufiventris)'),(4,'Guarupu (Melipona bicolor)');
/*!40000 ALTER TABLE `dtbAbelha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dtbPlantas`
--

DROP TABLE IF EXISTS `dtbPlantas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dtbPlantas` (
  `id_Planta` int NOT NULL AUTO_INCREMENT,
  `ds_Planta` varchar(25) DEFAULT NULL,
  `ds_MesFloracao` varchar(10) DEFAULT NULL,
  `id_Abelha` int DEFAULT NULL,
  PRIMARY KEY (`id_Planta`),
  KEY `id_Abelha` (`id_Abelha`),
  CONSTRAINT `dtbPlantas_ibfk_1` FOREIGN KEY (`id_Abelha`) REFERENCES `dtbAbelha` (`id_Abelha`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dtbPlantas`
--

LOCK TABLES `dtbPlantas` WRITE;
/*!40000 ALTER TABLE `dtbPlantas` DISABLE KEYS */;
INSERT INTO `dtbPlantas` VALUES (1,'Lírio da Paz','Janeiro',2),(2,'almoreira-negra','Junho',1),(3,'Pombeira','Novembro',3),(5,'Antúrio','Dezembro',4),(6,'Cúrcuma','Fevereiro',1),(7,'Bromélia','Março',2),(8,'Aleluia','Abril',3),(9,'Gazânia','Maio',4),(10,'Ipê-amarelo ','Julho',1),(11,'Dália','Agosto',1),(12,'Centáurea','Setembro',2),(13,'Hatiora rosea','Outubro',2);
/*!40000 ALTER TABLE `dtbPlantas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'cadPlanta'
--

--
-- Dumping routines for database 'cadPlanta'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-15 10:15:24
