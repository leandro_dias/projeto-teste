<?php 
	include "connect.php"
?>

<!DOCTYPE html>
<html>
<head>
	<link rel = "preconnect" href = "https://fonts.gstatic.com">
	<link href = "https://fonts.googleapis.com/css2? family = Asap: wght @ 700 & family = Nunito: wght @ 700 & family = Xanh + Mono & display = swap "rel =" stylesheet ">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>Cadastro de Plantas e Abelhas</title>
</head>
<body>

	<nav id="home">
		<a href="#">Cadastro de Plantas e Abelhas</a>
		<ul>
			<li><a href="index.php">Home</a></li>
			<li><a href="#cadastro">Cadastrar Planta</a></li>
		</ul>
	</nav>

		<section id="cadastro">
		<div>
			<h2>Cadastro de Plantas</h2>
			
			<form action="exCadastro.php" method="POST">

				<label class="label">Planta: </label>
				<div class="bloco">
					<input type="text" name="planta" class="select" maxlength="25" placeholder="Nome da Planta">
				</div>

				<label class="label">Mês de Floração: </label>
				<div class="bloco">
					<select class="select" name="mes">
						<option value="" disabled selected="">Selecione um mês</option>
						<option value="Janeiro">Janeiro</option>
						<option value="Fevereiro">Fevereiro</option>
						<option value="Março">Março</option>
						<option value="Abril">Abril</option>
						<option value="Maio">Maio</option>
						<option value="Junho">Junho</option>
						<option value="Julho">Julho</option>
						<option value="Agosto">Agosto</option>
						<option value="Setembro">Setembro</option>
						<option value="Outubro">Outubro</option>
						<option value="Novembro">Novembro</option>
						<option value="Dezembro">Dezembro</option>
					</select>
				</div>

				<label class="label">Tipo de Abelha: </label>	
				<div class="bloco">
					<select class="select" name="abelha">
						<option value="" disabled selected>
							Selecione uma abelha
						</option>

						<?php 

							$executa = "SELECT * FROM dtbAbelha";
							$query = $mysqli->query($executa);

							while ($dados=mysqli_fetch_row($query)) {
	                            
	                            $ds_abelha = $dados[1];
	                            $id_Abelha = $dados[0];        

	                            echo "<option value = ".$id_Abelha.">".$ds_abelha."</option>";
	                        }

						?>

					</select>
				</div>

				<input class="button" type="submit" name="Cadastrar">
			</form>
		
		</div>
		
	</section>

	<footer>
		<a href="#home">Voltar</a>
		<p>&copy;Desenvolvido por Leandro Dias</p>
	</footer>

</body>
</html>
