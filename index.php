<?php 
	include "connect.php"
?>

<!DOCTYPE html>
<html>
<head>
	<link rel = "preconnect" href = "https://fonts.gstatic.com">
	<link href = "https://fonts.googleapis.com/css2? family = Asap: wght @ 700 & family = Nunito: wght @ 700 & family = Xanh + Mono & display = swap "rel =" stylesheet ">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>Cadastro de Plantas e Abelhas</title>
</head>
<body>

	<nav id="home">
		<a href="#">Cadastro de Plantas e Abelhas</a>
		<ul>
			<li><a href="cadastro.php">Cadastrar Nova Planta</a></li>
			<li><a href="#consulta">Consultar Planta</a></li>
		</ul>
	</nav>

	<header id="topo">
		<h1></h1>
		<img class="ftCapa" src="img/capa.jpg" alt="Foto Capa">
	</header>

	<section id="consulta">
		<div>
			<h2>Consulta de Plantas</h2>
			
			<form action="index.php" method="POST">
				<label class="label">Tipo de Abelha: </label>
				<div class="bloco">	
					<select class="select" name="abelha">
						<option value="" disabled selected>
							Selecione uma abelha
						</option>

						<?php 

							$executa = "SELECT * FROM dtbAbelha";
							$query = $mysqli->query($executa);

							while ($dados=mysqli_fetch_row($query)) {
	                            
	                            $ds_abelha = $dados[1];
	                            $id_Abelha = $dados[0];   	                   

	                            echo "<option value = ".$id_Abelha.">".$ds_abelha."</option>";
	                        }

						?>

					</select>
				</div>

				<label class="label">Mês / Floração: </label>	
				<div class="bloco">
					<P><input class="select" type="checkbox" name="mes1" value="Janeiro"> Janeiro</P>
					<P><input class="select" type="checkbox" name="mes2" value="Fevereiro" > Fevereiro</P>
					<P><input class="select" type="checkbox" name="mes3" value="Março" > Março</P>
			
					<P><input class="select" type="checkbox" name="mes4" value="Abril" > Abril</P>
					<P><input class="select" type="checkbox" name="mes5" value="Maio" > Maio</P>
					<P><input class="select" type="checkbox" name="mes6" value="Junho" > Junho</P>

					<P><input class="select" type="checkbox" name="mes7" value="Julho" > Julho</P>
					<P><input class="select" type="checkbox" name="mes8" value="Agosto" > Agosto</P>
					<P><input class="select" type="checkbox" name="mes9" value="Setembro" > Setembro</P>
				
					<P><input class="select" type="checkbox" name="mes10" value="Outubro" > Outubro</P>
					<P><input class="select" type="checkbox" name="mes11" value="Novembro" > Novembro</P>
					<P><input class="select" type="checkbox" name="mes12" value="Dezembro" > Dezembro</P>
				</div>

				<input class="button" type="submit" name="Enviar">			

			</form>
			
		
		</div>
		
	</section>

	<section class="res">
		
		<div class="tab">
			<h2>Resultado da Pesquisa</h2>
				<?php

					//abelha
					$_POST['abelha'] = (isset($_POST['abelha']) ) ? $_POST['abelha'] : null;
					$abelha = $_POST['abelha'];
					
				
					//mes 1
					$_POST['mes1'] = (isset($_POST['mes1']) ) ? $_POST['mes1'] : null;
					$mes1 = $_POST['mes1'];
					//echo $mes1;
					
					//mes 2
					$_POST['mes2'] = (isset($_POST['mes2']) ) ? $_POST['mes2'] : null;
					$mes2 = $_POST['mes2'];
					//echo $mes2;
					
					//mes 3
					$_POST['mes3'] = (isset($_POST['mes3']) ) ? $_POST['mes3'] : null;
					$mes3 = $_POST['mes3'];
					//echo $mes3;
					
					//mes 4
					$_POST['mes4'] = (isset($_POST['mes4']) ) ? $_POST['mes4'] : null;
					$mes4 = $_POST['mes4'];
					//echo $mes4;

					//mes 5
					$_POST['mes5'] = (isset($_POST['mes5']) ) ? $_POST['mes5'] : null;
					$mes5 = $_POST['mes5'];
					//echo $mes5;

					//mes 6
					$_POST['mes6'] = (isset($_POST['mes6']) ) ? $_POST['mes6'] : null;
					$mes6 = $_POST['mes6'];
					//echo $mes6;

					//mes 7
					$_POST['mes7'] = (isset($_POST['mes7']) ) ? $_POST['mes7'] : null;
					$mes7 = $_POST['mes7'];
					//echo $mes7;

					//mes 8
					$_POST['mes8'] = (isset($_POST['mes8']) ) ? $_POST['mes8'] : null;
					$mes8 = $_POST['mes8'];
					//echo $mes8;

					//mes 9
					$_POST['mes9'] = (isset($_POST['mes9']) ) ? $_POST['mes9'] : null;
					$mes9 = $_POST['mes9'];
					//echo $mes9;

					//mes 10
					$_POST['mes10'] = (isset($_POST['mes10']) ) ? $_POST['mes10'] : null;
					$mes10 = $_POST['mes10'];
					// $mes10;

					//mes 11
					$_POST['mes11'] = (isset($_POST['mes11']) ) ? $_POST['mes11'] : null;
					$mes11 = $_POST['mes11'];
					//echo $mes11;

					//mes 12
					$_POST['mes12'] = (isset($_POST['mes12']) ) ? $_POST['mes12'] : null;
					$mes12 = $_POST['mes12'];
					//echo $mes12;

					//se nenhuma abelha for escolhida
					if(empty($abelha) ){
						$abelha = rand(1, 4);

						$executa = "SELECT
										PLANTA.ds_Planta			'Planta',
								        PLANTA.ds_MesFloracao		'Mes_Floracao',
								        ABELHA.id_Abelha			'id_Abelha',
								        ABELHA.ds_abelha			'Abelha'
									FROM
										dtbPlantas AS PLANTA INNER JOIN dtbAbelha AS ABELHA ON PLANTA.id_Abelha = ABELHA.id_Abelha
									WHERE  ABELHA.id_Abelha = '$abelha'";

						$query = $mysqli->query($executa);

					}

					//se uma abelha for escolhida
					if(!empty($abelha ) ){

						$executa = "SELECT
										PLANTA.ds_Planta			'Planta',
								        PLANTA.ds_MesFloracao		'Mes_Floracao',
								        ABELHA.id_Abelha			'id_Abelha',
								        ABELHA.ds_abelha			'Abelha'
									FROM
										dtbPlantas AS PLANTA INNER JOIN dtbAbelha AS ABELHA ON PLANTA.id_Abelha = ABELHA.id_Abelha
									WHERE  ABELHA.id_Abelha = '$abelha'";

						$query = $mysqli->query($executa);

					}

					//se nenhum mes for escolhido					
					if(empty($mes1) and empty($mes2) and empty($mes3) and empty($mes4) and empty($mes5) and empty($mes6) and empty($mes7) and empty($mes8) and empty($mes9) and empty($mes10) and empty($mes11) and empty($mes12) and (empty($abelha) )){

						$mesRand = rand(1, 12);
					

						$mes = 'mes'.$mesRand;

						switch ($mes) {
							case 'mes1':
								$mesR = 'Janeiro';
								break;
							case 'mes2':
								$mesR = 'Fevereiro';
								break;
							case 'mes3':
								$mesR = 'Março';
								break;
							case 'mes4':
								$mesR = 'Abril';
								break;
							case 'mes5':
								$mesR = 'Maio';
								break;
							case 'mes6':
								$mesR = 'Junho';
								break;
							case 'mes7':
								$mesR = 'Julho';
								break;
							case 'mes8':
								$mesR = 'Agosto';
								break;
							case 'mes9':
								$mesR = 'Setembro';
								break;
							case 'mes10':
								$mesR = 'Outubro';
								break;
							case 'mes11':
								$mesR = 'Novembro';
								break;
							case 'mes12':
								$mesR = 'Dezembro';
								break;
						}
					
						echo $mesR;

							$executa = "SELECT
											PLANTA.ds_Planta			'Planta',
									        PLANTA.ds_MesFloracao		'Mes_Floracao',
									        ABELHA.id_Abelha			'id_Abelha',
									        ABELHA.ds_abelha			'Abelha'
										FROM
											dtbPlantas AS PLANTA INNER JOIN dtbAbelha AS ABELHA ON PLANTA.id_Abelha = ABELHA.id_Abelha
										WHERE  PLANTA.ds_MesFloracao = '$mesR'";

							$query = $mysqli->query($executa);

					}

					
				?>



				<table align="center" border="1" border-radius="3">
					<tr>
						<th colspan="">Planta</th>
						<th colspan="">Mês Floração</th>
						<th colspan="">Abelha</th>
					</tr>
				<?php
					while ($dados=mysqli_fetch_object($query)) {
						echo "<tr> <td>".$dados->Planta . "</td>";
						echo "<td>".$dados->Mes_Floracao . "</td>";
						echo "<td>".$dados->Abelha . "</td>";
					}
					$query->free();

					echo "</table>";
				?>
				</div>

	</section>

	<footer>
		<a href="#home">Voltar</a>
		<p>&copy;Desenvolvido por Leandro Dias</p>
	</footer>

</body>
</html>