create database cadPlanta;

use cadPlanta;

create table dtbPlantas (
	id_Planta int auto_increment primary key,
    ds_Planta varchar(25),
    ds_MesFloracao varchar (10),
    id_Abelha int (2),
    foreign key (id_Abelha)
    references dtbAbelha (id_Abelha)
);
    
    
    create table dtbAbelha (
		id_Abelha int auto_increment primary key,
        ds_abelha varchar (40)
        );
        
	insert into dtbAbelha (ds_Abelha) values ('Iraí (Nannotrigona testaceicornes)');
	insert into dtbPlantas (ds_Planta, ds_MesFloracao, id_abelha) values ('almoreira-negra', 'Junho', '1');
    
    select * from dtbAbelha;
    select * from dtbPlantas;
    
    
    SELECT
		PLANTA.ds_Planta			'Planta',
        PLANTA.ds_MesFloracao		'Mês Floração',
        ABELHA.id_Abelha			'id Abelha',
        ABELHA.ds_abelha			'Abelha'
	FROM
		dtbPlantas AS PLANTA INNER JOIN dtbAbelha AS ABELHA ON PLANTA.id_Abelha = ABELHA.id_Abelha
	WHERE PLANTA.ds_MesFloracao = 'janeiro' and ABELHA.id_Abelha = 2;
        
        
        
delete from dtbPlantas where id_Planta = 10;
